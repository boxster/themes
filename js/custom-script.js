(function( $ ) {

    // Add Color Picker to all inputs that have 'color-field' class
    $(function() {
        $('.larryslist-color-field').wpColorPicker();
    });
     
})( jQuery );