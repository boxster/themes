== LarrysList ==
== Description == Larrys List is a directory or classified ads theme. Main listing page will show all latest listings and users can click-tap on picture to read full page description. Default sidebar has log in script and custom categories. The required plugin, tsw-custom-listing, is part of this theme and only needs to be installed and activated from the admin panel before you can list items as a directory or classified listing component. Compiled using Gridiculous Responsive framework. Makes it perfect for mobile, tablets and desktop. Options and features include Background, Header change; Sidebar widgets will show below theme-default sidebar widgets; Category and Archive search are separate from blog and listings search. Listings show only on template "View Listing" where blog posts show on assign blog page. Other features included are Email and social media links; Change anchor links colors; Remove and add new text to footer credits; Members have their own Dashboard with listing stats; Pop-up images of listing image; Front end form entry component.
Author: Larry Judd Oliver - Tradesouthwest
Author URI: http://themes.tradesouthwest.com/
Theme URI: http://themes.tradesouthwest.com/wordpress/larryslist/
Copyright: Larry Judd - Tradesouthwest, http://tradesouthwest.com
Version: 1.6
Tags: two-columns, right-sidebar, custom-header, custom-background, translation-ready, theme-options
License: GNU General Public License v3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Text Domain: larryslist


== Additional License ==
*  https://github.com/thomasgriffin/TGM-Plugin-Activation | @license http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
* images created with GIMP | GLP
* Gridiculous created by c.bavota | released under GPL v2 | March 4th, 2013
* Normalize.css v2.1.0 by Nicolas Gallagher - http://necolas.github.com/normalize.css/
* HTML5 Shiv v3.7.0 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed

== Instructions ==
* Please install the plugin or re-activate if you have switched themes.

* To make listings viewable create a page using the template, "View Listings" then add that page to your menu or make it the Static home page. "Recent Listings" is a good page name.

* To allow users to post listings from a front-end-post-form, simply add a new page using the template "Add Listing" then add that page to menu.

* Registered members will have their own Dashboard with Custom listing and Post access only.

* For more on How To Select Templates, see: http://en.support.wordpress.com/pages/page-attributes/

* for custom configuration or updates contact Larry at: tradesouthwest@gmail.com

== Change Log ==
1.7 Jun 19th 2017
* esc_url corrected

1.6 Jun 19th 2017
* added html5 support
* changed priority on content_width
* added local enqueue of html5shiv
* added core pingback support
* remove border from listing-view
* image height adjustments

1.5 may 9th 2017
* added img-center
* moved custom listing image to top of page
* widened page anchors multi-pager links
* added better gallery support
* fixed comments div

1.4 May 8th 2017
* corrected theme URI
* fixed responsive images
* fixed wp_calendar size
* removed some readme content
* tested on WP ver 4.7.4

1.2 July 6th 2015
* fixed a few text domains

1.1 June 3rd 2015
* added page links to single template
* added license for plugin activator

1.0 June 2nd 2015
* added core pagination to archive, search template
* removed unused images
* changed default-thumb image
* changed text in theme description

0.1 Jan. 31st 2015
first dev.
